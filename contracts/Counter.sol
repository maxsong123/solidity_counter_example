pragma solidity ^0.4.15;

// Increment by One contract, version 3

contract Counter {
  // state
  uint public counter;
  address public addr;


  // calculate and store the counter for the SC

  function add() public payable {    
    counter++;

  }

  function subtract() public {
    counter--;
  }
  // sent payment of ETH 
  // *transactional function*



  // reset the counter for the SC to 0
  // *transactional function*

  function reset() public {
    counter=0;
  }


  // helper function to get a counter
  // *read-only function*
  function readCounter() constant public returns (uint) {
    return counter;
  }



}
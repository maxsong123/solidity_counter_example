pragma solidity ^0.4.15;

// Increment by One contract, version 2

contract Counter3 {
  // state
  uint public counter;

  // calculate and store the counter for the SC
  // *transactional function*

  function add() public {
    counter++;
  }

  // calculate and store the counter for the SC
  // *transactional function*

  function subtract() public {
    counter--;
  }

  // reset the counter for the SC to 0
  // *transactional function*

  function reset() public {
    counter=0;
  }


  // helper function to get a counter
  // *read-only function*
  function readCounter() constant returns (uint) {
    return counter;
  }
}
pragma solidity ^0.4.15;

// Increment by One contract, version 3

contract Counter2 {
  // state
  uint public counter;
  address public addr;


  // calculate and store the counter for the SC

  function add() public payable {
    
    counter++;

    if(counter >=5) {
        require(addr!= address(0),'transfer needs a valid address');  
        addr.transfer(3);
        reset();
        } 
  }

  function subtract() public {
    counter--;
  }
  // sent payment of ETH 
  // *transactional function*



  // reset the counter for the SC to 0
  // *transactional function*

  function reset() public {
    counter=0;
  }


  // helper function to get a counter
  // *read-only function*
  function readCounter() constant returns (uint) {
    return counter;
  }

  // TXN Events 


  // set the target of the payout... 

  function set_target_address (address to_set_addr) public {
    addr = to_set_addr;
  }

  function read_target_address () public constant returns (address) {
    return addr;
  }

  function send(address _receiver) payable {
    _receiver.transfer(msg.value);
  }


}